# EC2 Ansible Deployer

Ansible for deploying [Ormuco App](https://gitlab.com/jibsonline/Ormuco) to AWS 

## Requirements
Python 2.7
Ansible 2.9+

## How to run:
```sh
ansible-playbook OrmucoApp-deploy.yml -i inventory/production/  -e version_to_deploy="<commit id to deploy>"
```
Eg: 
```sh
ansible-playbook OrmucoApp-deploy.yml -i inventory/production/ -e version_to_deploy="d53ac8fe"
```

> NB. Released Commit IDs can be found here --> https://gitlab.com/jibsonline/Ormuco/-/releases


## CI/CD Workflow

### Gitlab CI
I have used a combination of GitLab CI + Ansible. Every push/merge to the master branch will be zipped and published via [Ormuco GitLab CI](https://gitlab.com/jibsonline/Ormuco/-/blob/master/.gitlab-ci.yml) automation

### Ansible
The ansible scripts downloads the release based on the commit id provided and deploys the application on EC2 instances which are part of a target group and fronted by an ALB
